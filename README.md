- viewport : es el espacio disponible en la que se muestra la web
 - `<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />` solo tiene efectos en los navegadores movileis
- media queries
- modelos de cajas `box-sizing:border-box`
- medidas relativas (%,em,rem (root html))
- high density pixel
 - viewport != resolución de la pantalla
 - 1px viewport != 1px real
 - ejmplo js : `devicePixelRatio` imaginemos que sale 1.25 esto significa que un 1px del viewport
   equivale a 1.25px reales, si se hace zoom el valor aumentará, imaginemos que a 2.1875 entonces
   significará que un 1px del viewport equivale a 2.1875px reales de la pantalla

```css
 /*
  - min-width, max-width
  - orientation : landscape(si el width es mayor que el height) | portrait
  - min-aspect-ratio, max-aspect-ratio, device-aspect-ratio
 */
 @media screen and (max-width:1024px) and (orientation: landscape){

 }
```

Recomendacion :

- No usar em para el layout, para eso utilizar rem
- Usar em solo en componentes internos que varien

```css
  .button{
    font-size:3em;
    border:none;
    border-radius:.5em;
    background:red;
    color:#fff;
    padding: .5em 1.5em;
    margin:2rem;
  }
```

Unidades relativas
1. porcentajes %
2. em y rem

1em = 100% del tamaño de fuente en el contexto
1rem = 100% del tamaño de fuente del root `<html>`

3. unidades de viewport : vw ,vh , vmin, vmax
1vw = 1% del ancho del viewport
1vh = 1% del alto del viewport
1vmin = 1% del lado menor del viewport (puede ser el height o el width)
1vmax = 1% del lado mayor del viewport (puede ser el height o el width

- CSSOM

````js

const root = document.documentElement // html
const rootSyles = getComputedStyle(root)
console.log("rootSyles", rootSyles.getPropertyValue('--bg'))

/**
 * getComputedSytle => CSSOM equivalente al DOM pero para css
 */
/*


const rootStyle = document.documentElement.style

const getScrollBarWidth = () => innerWidth - document.documentElement.clientWidth

rootStyle.setProperty('--scroll-bar-width',getScrollBarWidth())




*/
// manipular variables css con javascript

// root
// document.documentElement
// document.documentElement.style.setProperty('--color', e.target.value)
// const rootSyles = getComputedStyle(root)
// rootSyles.getPropertyValue('--bg')

// otra forma

// const root = document.querySelector(':root');
// const rootStyles = getComputedStyle(root);
// const pink_color = rootStyles.getPropertyValue('--pink');
// root.style.setProperty('--pink', '#F7F00C')


// window.getComputedStyle(document.querySelector('.banner-container')) // CSSOM

/*

document.body.classList.add('name-style')

let size = 2 // dinamic
const styles = `
 background : ${color};
 border-bottom: ${size * 2}px solid black;
`
// lo que hara esto es reemplazar los estilos inline existentes, es decir esto seria como un reset
element.setAttribute('style',styles)

// lo que hara es agremar mas estilos inline existentes, esto seria como un append
element.style += `;${styles}`


*/

const letterStyles= `
        margin-right: -0.6em;
        transform : scaleY(0) translateY(-40px);
        display : inline-block;
        transition : all 0.2s cubic-bezier(0.3,0,0.3,1);
    `,
  wordStyles= `
        margin-right:0.5em;
    `;

document.getElementById('id').style = letterStyles
document.getElementById('id').innerHTML = `
  <span
      style='${letterStyles}'
      class='letter'
    ></span>`
const span = document.createElement("span");
span.style.cssText = wordStyles;

````

