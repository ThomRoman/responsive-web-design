¿Qué es PostCSS?

- PostCSS por sí solo es simplemente una API
- Es una herramienta para transformar estilos mediante plugins de JS
- Es un ecosistemas de plugins custom y herramientas
- Utiliza Node.js
- pensemos en PostCSS como si fuera Babel para CSS
- lo que hace es tomar nuestro styles.css y todo agregarlo a un objeto js
- no es un post-procesador

Limitaciones de los preprocesadores

- no son extendibles: limitados a los que provee la versiona ctual
- se carga todo el código que utulizan: funcionalidad como @extend
  por mas que no las utilicemos no pueden ser removidad para reducir
  código
- no puden ser utilizados como polyfills de testeo de nuevos standars de W3C
- aca es donde entra PostCSS

¿Por qué usar PostCSS?

- es una herramienta para transformar estilos mediante plugins de js. Estos
  plugins pueden hacer lint de tu CSS, soportar variables y mixins, transpilar
  sintaxis de CSS futura de CSS 4, grillas, gradientes cónicos y mucho mas.
- se sigue escribiendo css plano
- postcss es agnóstico al formato del lenguaje, sí se puede usar funto a sass `.scss`
 - para poder usarlo con sass - plugin PreCSS
 - tambien se puede usar con Webpack y combinar PostCSS y SCSS
- autoprefixer es un postcss
 - es un plugin PostCSS que analiza y añade los prefijos de los navegadores a las
   propiedades CSS necesarias, usando los valores de Can I Use
- PostCSS es un módulo de Node.js que analiza tu CSS y lo transforma
  en un árbol de sintaxis abstracta (AST)
- Ese AST es interpretado y puede ser modificado mediante funciones o plugins Javascript.
- Luego, PostCSS convierte nuevamente ese AST en texto, generando un archivo de salida
  con un css modificado

```css
 :root{
  --darger-theme: {
   color: white;
   background-color: red;
  };
 }
 .danger{
  @apply --danger-theme;
 }
```

```js
 // WEBPACK

 // modules
 module.exports = {
  module:{
   rules:[
    {
      test: /\.css$/,
      exclude: /node_modules/,
      use: [
       {
         loader: 'style-loader',
       },
       {
         loader: 'css-loader',
         options: {
          importLoader: 1,
         }
       },
       {
         loader: 'postcss-loader',
       },
      ]
    }
   ]
 }
}

 // plugins
 module.exports = {
   plugins: [
     require('precss'),
     require('autoprefixer')
   ]
 }

```

```sh
  npm i (-g | -D) postcss-cli
  postcss --use autoprefixer -c options.json -o main.css css/*.css
```

¿Qué no es PostCSS ?

- no es un preprocesador
- no es un postprocesador
- no es la sintaxis del futuro
- no es una herramienta de limpieza y/u optimización

- https://cssnext.github.io/playground/ `@use cssnext;` - cssnext obsoleto, usar `postcss-preset-env`
- http://slides.com/joanleon/postcss#/
- https://sneakertack.github.io/postcss-playground/
- https://www.youtube.com/watch?v=iUUB5sUk_ZA